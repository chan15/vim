# nodejs

## fixjson
npm i -g fixjson

## eslint
npm i -g eslint eslint-config-airbnb-base eslint-plugin-import

# php

## php-cs-fixer
composer global require friendsofphp/php-cs-fixer

# python

## autopep8
pip install autopep8
