set nocompatible
filetype off

set rtp+=$HOME/vimfiles/bundle/Vundle.vim
call vundle#begin('$USERPROFILE/vimfiles/bundle/')
Plugin 'editorconfig/editorconfig-vim'
Plugin 'DataWraith/auto_mkdir'
Plugin 'Lokaltog/vim-easymotion'
Plugin 'MarcWeber/vim-addon-mw-utils'
Plugin 'StanAngeloff/php.vim'
Plugin 'VundleVim/Vundle.vim'
Plugin 'adoy/vim-php-refactoring-toolbox'
Plugin 'ap/vim-css-color'
Plugin 'ntpeters/vim-better-whitespace'
Plugin 'captbaritone/better-indent-support-for-php-with-html'
Plugin 'chase/vim-ansible-yaml'
Plugin 'ekalinin/dockerfile.vim'
Plugin 'evidens/vim-twig'
Plugin 'garbas/vim-snipmate'
Plugin 'godlygeek/tabular'
Plugin 'gregsexton/MatchTag'
Plugin 'honza/vim-snippets'
Plugin 'jiangmiao/auto-pairs'
Plugin 'kien/ctrlp.vim'
Plugin 'majutsushi/tagbar'
Plugin 'maksimr/vim-jsbeautify'
Plugin 'mattn/emmet-vim'
Plugin 'nanotech/jellybeans.vim'
Plugin 'nathanaelkane/vim-indent-guides'
Plugin 'nginx.vim'
Plugin 'nlknguyen/vim-docker-compose-syntax'
Plugin 'pangloss/vim-javascript'
Plugin 'pearofducks/ansible-vim'
Plugin 'rhysd/conflict-marker.vim'
Plugin 'rhysd/vim-fixjson'
Plugin 'scrooloose/nerdtree'
Plugin 'stephpy/vim-php-cs-fixer'
Plugin 'tell-k/vim-autopep8'
Plugin 'tmhedberg/matchit'
Plugin 'tomtom/tcomment_vim'
Plugin 'tomtom/tlib_vim'
Plugin 'tpope/vim-surround'
Plugin 'tpope/vim-vinegar'
Plugin 'vim-airline/vim-airline'
Plugin 'vim-scripts/AutoComplPop'
Plugin 'vim-scripts/IndexedSearch'
Plugin 'vim-scripts/SearchComplete'
Plugin 'vim-scripts/indentpython.vim'
Plugin 'vim-scripts/restore_view.vim'
Plugin 'vim-scripts/smarty-syntax'
Plugin 'w0rp/ale'
Plugin 'whatyouhide/vim-lengthmatters'
Plugin 'xolox/vim-misc'
Plugin 'xolox/vim-reload'
call vundle#end()

syntax on
filetype plugin indent on
filetype plugin on
source $VIMRUNTIME/delmenu.vim
language messages zh_TW.utf-8
autocmd FileType php set omnifunc=phpcomplete#CompletePHP
autocmd FileType yaml setlocal ts=2 sts=2 sw=2 expandtab
au BufRead,BufNewFile *.tpl set filetype=smarty.html
silent! colorscheme jellybeans

set autoindent
set cursorline
set encoding=utf-8
set expandtab
set fileencoding=utf-8
set fileencodings=utf-8,big5,euc-jp,gbk,euc-kr,utf-bom,iso8859-1
set foldcolumn=1
set guifont=Consolas:h12
set laststatus=2
set nobackup
set noundofile
set nowritebackup
set number
set shellslash
set shiftwidth=4
set tabstop=4
set tenc=utf-8
set winheight=5
set winminheight=5
set winheight=999
set hlsearch
set incsearch
set wildignore+=*\\tmp\\*,*.swp,*.zip,*.exe  " Windows
set guioptions-=e
set fileformat=unix
set fileformats=unix,dos

let g:EasyMotion_leader_key = '<Leader>'
let g:NERDTreeHijackNetrw = 0
let g:airline_powerline_fonts = 1
let g:ale_lint_on_enter = 0
let g:ale_lint_on_text_changed = 'never'
let g:ctrlp_custom_ignore = 'node_modules\|vendor\|git'
let g:ctrlp_working_path_mode = 0
let g:ctrlp_max_files = 0
let g:ctrlp_max_depth = 40
let g:lengthmatters_start_at_column = 121
let g:vim_php_refactoring_auto_validate_rename = 1
let g:vim_php_refactoring_auto_validate_sg = 1
let g:vim_php_refactoring_auto_validate_visibility = 1
let g:fixjson_fix_on_save = 0
let g:indent_guides_guide_size = 1
let g:indent_guides_start_level = 2
let g:indent_guides_enable_on_vim_startup = 1
let g:snipMate = { 'snippet_version' : 1  }

map <C-t> :tabnew<CR>
map <C-l> :tabnext<CR>
map <C-h> :tabprevious<CR>
map <F2> :NERDTreeToggle<CR>
map <F3> :NERDTreeFind<CR>
nmap <F4> :TagbarToggle<CR>
map <C-\> :tab split<CR>:exec("tag ".expand("<cword>"))<CR>
map <Leader>t :!ctags -R --languages=javascript,php,python<CR>
nmap <c-]> :tab tag <c-r><c-w><cr>
nmap <silent> <C-k> <Plug>(ale_previous_wrap)
nmap <silent> <C-j> <Plug>(ale_next_wrap)
