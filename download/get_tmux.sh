#!/bin/bash
# curl -sL "https://vim.chan15.info/download/get_tmux.sh" | bash

yellow='\033[1;33m'
no_color='\033[0m' # No Color
bashrc_config='alias tmux="tmux -2"'
tmux_config=$(cat << EOF
# ~/.tmux.conf
setw -g mode-keys vi

# List of plugins
set -g @plugin 'tmux-plugins/tpm'
set -g @plugin 'tmux-plugins/tmux-sensible'
set -g @plugin 'dracula/tmux'
set -g @dracula-plugins "git cpu-usage ram-usage time"
set -g @dracula-show-powerline true

# Initialize TMUX plugin manager (keep this line at the very bottom of tmux.conf)
run -b '~/.tmux/plugins/tpm/tpm'
EOF
)

config_not_in_bashrc() {
    counter=$(cat ~/.bashrc | grep "$bashrc_config" | wc -l)

    if [ "$counter" -gt 0 ]; then
        return 1
    fi

    return 0
}

dracula_not_in_tmux_conf() {
    counter=$(cat ~/.tmux.conf | grep "dracula/tmux" | wc -l)

    if [ "$counter" -gt 0 ]; then
        return 1
    fi

    return 0
}

insert_tmux_conf() {
    echo "$tmux_config" >> ~/.tmux.conf
}

get_distribution() {
    lsb_dist=""
    if [ -r /etc/os-release ]; then
        lsb_dist="$(. /etc/os-release && echo "$ID")"
    fi
    echo "$lsb_dist"
}

dispatch() {
    cd /tmp

    if [ ! -d /tmp/tmux ]; then
        git clone https://github.com/tmux/tmux.git
    fi

    cd tmux
    sudo sh autogen.sh
    sudo ./configure && sudo make
    sudo make install
}

go_ubuntu() {
    echo install tmux in ubuntu
    sudo apt purge -y tmux
    sudo apt install -y git make gcc autoconf automake pkg-config libevent-dev libncurses-dev bison

    dispatch
}

go_centos() {
    echo install tmux in centos
    sudo yum install -y git
    sudo yum remove -y tmux
    sudo yum install -y git make gcc autoconf automake pkg-config bison libevent-devel ncurses-devel

    dispatch
}

distribution=$(get_distribution)
case "$distribution" in
    ubuntu)
        go_ubuntu
        ;;
    centos)
        go_centos
        ;;
    *)
        echo not compatible OS
esac

if [ ! -f ~/.tmux/plugins/tpm ]; then
    git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm
fi

if [ ! -f ~/.tmux.conf ]; then
    touch ~/.tmux.conf
    insert_tmux_conf
else
    if dracula_not_in_tmux_conf; then
        insert_tmux_conf
    fi
fi

if config_not_in_bashrc; then
    echo -e "\n$bashrc_config" >> ~/.bashrc
    source ~/.bashrc
fi

echo -e "${yellow}$(tmux -V)${no_color} installed"
