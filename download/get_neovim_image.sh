#!/bin/bash
# curl -sL "https://vim.chan15.info/download/get_neovim.sh" | bash

info() {
  yellow='\033[1;33m'
  no_color='\033[0m' # No Color
    echo -e "${yellow}$1${no_color}"
}

start() {
  info "Downloading Neovim AppImage..."
  curl -LO https://github.com/neovim/neovim/releases/download/stable/nvim.appimage
  chmod +x nvim.appimage

  sudo mv nvim.appimage /usr/local/bin/nvim

  sh -c 'curl -fLo "${XDG_DATA_HOME:-$HOME/.local/share}"/nvim/site/autoload/plug.vim --create-dirs \
      https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
  mkdir -p  ~/.config/nvim/plugged
  curl -sL -o ~/.config/nvim/init.vim https://vim.chan15.info/download/neovim_init_on_linux_by_vim_plug.txt
  nvim +PlugInstall +qall

  info "$(nvim -v | head -n 1) installed"
}

check_fuse() {
  if [ -f /etc/lsb-release ]; then
      INSTALL_CMD="apt-get"
  elif [ -f /etc/redhat-release ]; then
      INSTALL_CMD="yum"
  else
      info "Supported distributions: Ubuntu, CentOS"
      exit 1
  fi

  if ! command -v fuse &> /dev/null; then
      info "Installing FUSE..."

      sudo ${INSTALL_CMD} install -y fuse
  fi

  if ! command -v git &> /dev/null; then
      info "Installing git..."

      sudo ${INSTALL_CMD} install -y git
  fi
}

check_fuse
start
