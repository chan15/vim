#!/bin/bash

need_install_docker=0
need_install_docker_compose=0

install_docker() {
  echo "Install docker"
  need_install_docker=1
  curl -sL get.docker.com | sudo bash
  getent group docker || sudo groupadd docker
  id -nG "$USER" | grep -qw docker || sudo gpasswd -a "$USER" docker
}

install_docker_compose() {
  echo "Install docker compose"
  need_install_docker_compose=1

  tag=$(git -c 'versionsort.suffix=-' \
    ls-remote --exit-code --refs --sort='version:refname' --tags https://github.com/docker/compose/ '*.*.*' |
    tail --lines=1 |
    cut --delimiter='/' --fields=3)
  url="https://github.com/docker/compose/releases/download/$tag/docker-compose-linux-x86_64"
  composer_path="/usr/bin/docker-compose"

  sudo curl -sL "$url" -o "$composer_path"
  sudo chmod +x "$composer_path"
}

if ! command -v "docker" &>/dev/null; then
  install_docker
fi

if ! command -v "docker-compose" &>/dev/null; then
  install_docker_compose
fi

while [ "$1" != "" ]; do
  case "$1" in
  --docker)
    install_docker
    ;;
  --docker-compose)
    install_docker_compose
    ;;
  esac
  shift
done

if [[ "$need_install_docker" == 0 && "$need_install_docker_compose" == 0 ]]; then
  echo "Nothing installed, you can add --docker or --docker-compose to reinstall"
fi
