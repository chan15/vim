#!/bin/bash
# curl -sL "https://vim.chan15.info/download/get_neovim.sh" | bash

yellow='\033[1;33m'
no_color='\033[0m' # No Color

get_distribution() {
    lsb_dist=""
    if [ -r /etc/os-release ]; then
        lsb_dist="$(. /etc/os-release && echo "$ID")"
    fi
    echo "$lsb_dist"
}

get_version() {
    version=""
    if [ -r /etc/os-release ]; then
        version="$(. /etc/os-release && echo "$VERSION_ID")"
    fi
    echo "$version"
}

dispatch() {
    sh -c 'curl -fLo "${XDG_DATA_HOME:-$HOME/.local/share}"/nvim/site/autoload/plug.vim --create-dirs \
        https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
    mkdir -p  ~/.config/nvim/plugged
    curl -sL -o ~/.config/nvim/init.vim https://vim.chan15.info/download/neovim_init_on_linux_by_vim_plug.txt
    nvim +PlugInstall +qall

    echo -e "${yellow}$(nvim -v | head -n 1)${no_color} installed"
}

go_ubuntu() {
    echo install neovim in ubuntu
    sudo apt install -y git
    sudo add-apt-repository ppa:neovim-ppa/unstable
    sudo apt update
    sudo apt install -y neovim

    dispatch
}

go_centos7() {
    echo install neovim in centos7
    sudo yum install -y git
    sudo yum -y install epel-release
    sudo curl -o /etc/yum.repos.d/dperson-neovim-epel-7.repo https://copr.fedorainfracloud.org/coprs/dperson/neovim/repo/epel-7/dperson-neovim-epel-7.repo 
    sudo yum -y install neovim --enablerepo=epel

    dispatch
}

go_centos8() {
    echo install in centos8
    sudo yum install -y git
    sudo yum install -y https://dl.fedoraproject.org/pub/epel/epel-release-latest-8.noarch.rpm
    sudo yum install -y neovim python3-neovim

    dispatch
}

distribution=$(get_distribution)
version=$(get_version)
case "$distribution" in
    ubuntu)
        go_ubuntu
        ;;
    centos)
        case "$version" in
            7)
                go_centos7
                ;;
            8)
                go_centos8
                ;;
            *)
                echo no compatible version
        esac
        ;;
    *)
        echo not compatible OS
esac
