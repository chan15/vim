localedef -i zh_TW -c -f UTF-8 zh_TW.UTF-8
yum -y install vim
yum -y install git
mkdir -p ~/.vim/bundle
git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
curl -o ~/.vimrc https://vim.chan15.info/download/vimrc_on_linux_by_vundle.txt
vim +PluginInstall +qall
